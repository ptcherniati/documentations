# Installations

## Installation java
### java 8

``` sh 
	sudo apt-get install openjdk-8-jre
```
On peut alors tester 

``` sh 
java -version;

cat <<EOF > HelloWorld.java
class HelloWorld {

    public static void main(String args[]){
        System.out.println("Bonjour tout le monde!");
   }
 }
EOF

javac HelloWorld.java;
java HelloWorld;
rm HelloWorld.*

```

### java 11

``` sh 
	sudo apt-get install openjdk-11-jre
```
On peut alors tester 

``` sh 
java -version;

cat <<EOF > HelloWorld.java
class HelloWorld {

    public static void main(String args[]){
        System.out.println("Bonjour tout le monde!");
   }
 }
EOF

javac HelloWorld.java;
java HelloWorld;
rm HelloWorld.*
```


### Changer de configuration java 

``` sh 
sudo update-alternatives --config java
```
## Installation maven

``` sh 
sudo apt-get install maven
```
On peut alors tester 

``` sh 
mvn --version
```

## Configuration SSH

On va créer une clef publique. La bonne pratique est d'en créer une par client.
``` sh
ssh-keygen
```

On trouvera la clef publique dans le répertoire .ssh de home

## Installation de Docker 

https://docs.docker.com/engine/install/ubuntu/

- on enlève éventuelles versions


``` sh 
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
```

- on installe les librairies requises


``` sh 
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

- On ajoute la clef du repository docker


``` sh 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```


- On installe Docker


``` sh 
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

- On teste

``` sh 
sudo apt-get update
sudo docker run --rm hello-world
```

- on s'ajoute au groupe docker 

``` sh 
sudo groupadd docker
sudo usermod -aG docker $USER
``` 

- On redemarre la session et on teste (sans sudo)

``` sh 
docker run --rm hello-world
```
 - Demarrage de docker au démarrage 
 

``` sh 
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
```
## Installation de docker-compose

``` sh 
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

```
- On teste

``` sh 
docker-compose --version
```


## Installation NodeJS


``` sh 
sudo apt-get update
sudo apt-get install nodejs npm libnode64
```
- On teste

``` sh 
cat <<EOF |node -
var http = require('http');
var port = 8089;
http.createServer(function (req, res) {
res.writeHead(200, {'Content-Type': 'text/html'});
res.write('<h1 style="text-align: center;">Bonjour Lucile !<br/></h1>');
res.write('<p style="align-items: center; display: flex; color: red; font-size: 2em; ">Taper <img style="width: 5em;" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLizAMqCfaQvu6DCwiOKTEuHXAvQTcrgJq6w&usqp=CAU"/> dans la console pour quitter<br/>');
res.end();
}).listen(port);
console.log("Serveur tourne sur http://localhost:"+port);
EOF

```

## Installation de Git



``` sh 
sudo apt-get install git
```
- On teste

``` sh
git --version
```
- On configure git  (remplacer par les valeurs de l'utilisateur)

``` sh
git config --global user.name "Prénom nom"
git config --global user.email "prenom.nom@serveur.com"
```

- On teste

``` sh
git config --global --list
```
Ceci permet de marquer automatiquement les commits avec ces informations.
D'autres configurations sur https://git-scm.com/book/fr/v2/Personnalisation-de-Git-Configuration-de-Git

## installation de cUrl

``` sh
sudo apt-get install curl
```

## installation de PostgreSQL et PGAdmin

On peut suivre les procédures [postgreSQL](https://doc.ubuntu-fr.org/postgresql)  et [PGAdmin](https://devstory.net/11353/installer-pgadmin-sur-ubuntu#a6605357).

On peut aussi installer des containers docker en utilisant docker-compose.

A tester 

### pour portainer

``` sh
mkdir portainer
cd portainer
curl https://ptcherniati.pages.mia.inra.fr/documentations/docker/portainer/docker-compose.yml>docker-compose.yml
docker-compose up -d 
``` 
on vérifiéra en se connectant à l'adresse [http://localhost:9000]

### pour PostgreSQL

``` sh
cd ../
mkdir postrges
cd postgres
curl https://ptcherniati.pages.mia.inra.fr/documentations/docker/postgres/docker-compose.yml>docker-compose.yml
docker-compose up -d 
```

#  EDI

## IntelliJ

https://www.jetbrains.com/fr-fr/idea/download/#section=linux

## Visual Studio Code
https://code.visualstudio.com/Download

##NetBeans

http://netbeans.apache.org/download/nb123/nb123.html

# Outils

## SublimeMerge (Git client)

``` sh
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-get install apt-transport-https

echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list

sudo apt-get update
sudo apt-get install sublime-merge
```

## Kate (editeur de texte)

``` sh
sudo apt-get-install kate
```

## Remina  (ssh)

``` sh
sudo apt-get install remina
```


## Insomnia (API Client)

``` sh
sudo apt-get install insomnia
```

## GhostWriter (editeur markdown)

``` sh
sudo apt-get install ghostwriter
```

## GhostWriter (editeur markdown)

``` sh
sudo apt-get install ghostwriter
```

## Pandoc (convertisseur multiformat)

``` sh
sudo apt-get install pandoc
```


## Zoom (visiophonie)
